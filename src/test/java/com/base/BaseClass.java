package com.base;

import com.compHerokuAuto.Hooks;
import com.pages.AddNewComputerPage;
import com.pages.ListOfComputerPage;
import org.openqa.selenium.WebDriver;

/**
 * Created by Abdul on 10/04/2017.
 */
public class BaseClass {


    protected WebDriver driver;
    public AddNewComputerPage addNewComputerPage;
    public ListOfComputerPage listOfComputerPage;



    public BaseClass(){
        this.driver = Hooks.driver;
        addNewComputerPage = new AddNewComputerPage(driver);
        listOfComputerPage = new ListOfComputerPage(driver);


    }

}

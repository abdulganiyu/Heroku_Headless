package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by Abdul on 10/04/2017.
 */
public class ListOfComputerPage {

    protected WebDriver driver;

    public ListOfComputerPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    @FindBy(id = "add")
    public static WebElement addNewComputerBtn;
    @FindBy(id = "name")
    public static WebElement computerName;
    @FindBy(id = "introduced")
    public static WebElement introducedDate;
    @FindBy(id = "discontinued")
    public static WebElement discontinuedDate;

    @FindBy(id = "company")
    WebElement chooseCompany;
    @FindBy(xpath = "html/body/section/form/div/input")
    public static WebElement createBtn;

    @FindBy(xpath =  "//*[@id=\"main\"]/div[1]/strong")
    public WebElement alertMessage;

    public void companyName() {
        Select sel = new Select(chooseCompany);
        sel.selectByValue("1");

    }

    public void checkMasg(String text) {
       // driver.findElement(By.id("")).sendKeys("");

        Assert.assertTrue(alertMessage.getText().equals(text));

    }
}
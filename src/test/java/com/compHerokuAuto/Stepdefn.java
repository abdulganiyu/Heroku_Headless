package com.compHerokuAuto;

import com.base.BaseClass;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;

import static com.pages.ListOfComputerPage.*;

/**
 * Created by Abdul on 10/04/2017.
 */
public class Stepdefn extends BaseClass {

    WebDriver driver;


    public Stepdefn(){
        this.driver = Hooks.driver;

    }


    @Given("^I am on Play sample application — Computer database page$")
    public void i_am_on_Play_sample_application_Computer_database_page() throws Throwable {

        driver.get("http://computer-database.herokuapp.com/computers");

    }

    @Then("^I click on Add a new computer button$")
    public void i_click_on_Add_a_new_computer_button() throws Throwable {

        addNewComputerBtn.click();

    }

    @And("^I enter the details \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" in input fields$")
    public void i_enter_the_details_and_and_in_input_fields(String arg1, String arg2, String arg3) throws Throwable {

        computerName.sendKeys(arg1);
        introducedDate.sendKeys(arg2);
        discontinuedDate.sendKeys(arg3);

    }

    @And("^I select the company in the drop down list$")
    public void i_select_the_company_in_the_drop_down_list() throws Throwable {

        listOfComputerPage.companyName();

    }

    @When("^I click creat this computer button$")
    public void i_click_creat_this_computer_button() throws Throwable {

        createBtn.click();

    }

    @Then("^I should see \"([^\"]*)\"$")
    public void i_should_see(String test) throws Throwable {
        listOfComputerPage.checkMasg(test);
    }
//  second scenario

    @Then("^I enter the computer name in the search input box$")
    public void i_enter_the_computer_name_in_the_search_input_box() throws Throwable {

    }

    @And("^I click Filter by name button$")
    public void i_click_Filter_by_name_button() throws Throwable {

    }

    @Then("^I should see computer details to be read$")
    public void i_should_see_computer_details_to_be_read() throws Throwable {

    }


//    3rd scenario

    @And("^I Click on the computer to be updated$")
    public void i_Click_on_the_computer_to_be_updated() throws Throwable {

    }

    @And("^I Edit / input details in the input field\\(s\\)$")
    public void i_Edit_input_details_in_the_input_field_s() throws Throwable {

    }

    @When("^I Click on save this computer$")
    public void i_Click_on_save_this_computer() throws Throwable {

    }

    // 4th scenario

    @And("^I Click on the computer to be to be deleted$")
    public void i_Click_on_the_computer_to_be_to_be_deleted() throws Throwable {

    }

    @And("^I click delete button$")
    public void i_click_delete_button() throws Throwable {

    }


}

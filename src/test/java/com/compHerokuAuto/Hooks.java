package com.compHerokuAuto;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by Abdul on 10/04/2017.
 */
public class Hooks {

    public static WebDriver driver;

    @Before
    public WebDriver setup(){



       //  ChromeOptions options= new ChromeOptions();
      //  options.addArguments("--disable-infobars");
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\Sekinat\\Documents\\Selenium_Jars\\geckodriver.exe");
       // System.setProperty("webdriver.chrome.driver", "C:\\Users\\Sekinat\\Documents\\Selenium_Jars\\chromedriver.exe");
        // driver = new ChromeDriver(options);
       // driver = new ChromeDriver();
       driver = new FirefoxDriver();

       // driver = new HtmlUnitDriver();

       // driver.manage().window().maximize();
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        return driver;
    }

    @After
    public void teardown()
    {
        driver.quit();
    }

}
